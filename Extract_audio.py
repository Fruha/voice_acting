import moviepy.editor
import speech_recognition as speech_recog
import os
from pydub import AudioSegment

def extract_audio_from_video(path_video):
	name = path_video.split('/')[-1].split('.')[0]
	video = moviepy.editor.VideoFileClip(path_video)
	audio = video.audio
	audio.write_audiofile(f"qwe/sounds/{name}.wav")


for i in os.listdir('qwe/videos'):
    extract_audio_from_video(f'qwe/videos/{i}')
