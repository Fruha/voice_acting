import wave
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import math
import moviepy.editor
from moviepy.editor import AudioFileClip, concatenate_audioclips	


def cut_audio(start, end):
    global clips, audioclip
    clips.append(audioclip.subclip(start,end))

def join_audio(text = 'test_cut_audio1.wav'):
    final_clip = concatenate_audioclips(clips)
    final_clip.write_audiofile(text)


# audioclip = AudioFileClip("jirik1_cut1min.wav")
# clips = []


# cuting
# cut_audio(51.7,52.43)
# cut_audio(0.83,0.925)
# cut_audio(0.974,1.15)
# join_audio()

def extract_audio_from_video(path_video):
	name = path_video.split('/')[-1].split('.')[0]
	video = moviepy.editor.VideoFileClip(path_video)
	audio = video.audio
	audio.write_audiofile(f"qwe/sounds/{name}.wav")

extract_audio_from_video('qwe/videos/my_concatenation.mp4')
