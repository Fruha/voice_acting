import requests
import re
from bs4 import BeautifulSoup as BS
from moviepy.editor import VideoFileClip, concatenate_videoclips
import moviepy.editor
import time
import speech_recognition as speech_recog
import os
from pydub import AudioSegment

def rename_files(path):
    
    path = 'qwe'
    for file in os.listdir(path):
        try:
            os.rename(f'{path}/{file}', f'{path}/{recognize_audio(f"{path}/{file}")}.{file.split(".")[-1]}')
        except:
            pass

def recognize_audio(audio_file):
    silence = AudioSegment.from_wav("Silence.wav")
    sound_test = AudioSegment.from_wav(audio_file)

    combined_sounds = sound_test + silence
    combined_sounds.export("test.wav", format="wav")
    audio_file = 'test.wav'
    sample_audio = speech_recog.AudioFile(audio_file)
    recog = speech_recog.Recognizer()
    with sample_audio as audio_file:
        audio_content = recog.record(audio_file)

    return recog.recognize_google(audio_content, language = 'ru_RU').lower()

def print_arr(arr):
    i = 0
    for elem in arr:
        print(i,elem)
        i += 1

def cut_video(start, end):
    global video, clips
    clip = video.subclip(start, end)
    clips.append(clip)

def create_video(clips, text ='my_concatenation.mp4' ):
    final_clip = concatenate_videoclips(clips)
    final_clip.write_videofile(text)

def read_from_file(path_txt):
    f = open(path_txt, 'r')
    ans = []
    temp = (f.read().split('\n'))
    for i in temp[:-1]:
        ans.append(i.split('\t'))
    return ans

def split_video(path_video):
    name = path_video.split('/')[-1].split('.')[0]
    labels = read_from_file('qwe/sounds/{name}.txt')
    video = moviepy.editor.VideoFileClip(path_video)
    for i in labels:
        try:
            print(i)
            temp_video = video.subclip(float(i[0]), float(i[1]))
            temp_audio = temp_video.audio
            temp_audio.write_audiofile('temp_audio.wav')
            text = recognize_audio('temp_audio.wav')
            temp_video.write_videofile(f'qwe/short_videos/{text}.mp4')
        except:
            pass
        

# clips = []
# video = moviepy.editor.VideoFileClip("jirik1.mp4")
# url_video  = 'https://www.youtube.com/watch?v=5ZMIR1fEnOg'

# videoclip = VideoFileClip("jirik1.mp4")
# audioclip = videoclip.audio.write_audiofile('jirik1.wav')
# exit()

for i in os.listdir('qwe/videos')
    split_video('qwe/videos/{i}.mp4')

print('-------')