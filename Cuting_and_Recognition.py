import moviepy.editor
import speech_recognition as speech_recog
import os
from pydub import AudioSegment


def recognize_audio(audio_file):
    silence = AudioSegment.from_wav("Silence.wav")
    sound_test = AudioSegment.from_wav(audio_file)

    combined_sounds = sound_test + silence
    combined_sounds.export("test.wav", format="wav")
    audio_file = 'test.wav'
    sample_audio = speech_recog.AudioFile(audio_file)
    recog = speech_recog.Recognizer()
    with sample_audio as audio_file:
        audio_content = recog.record(audio_file)

    return recog.recognize_google(audio_content, language = 'ru_RU').lower()

def read_from_file(path_txt):
    f = open(path_txt, 'r')
    ans = []
    temp = (f.read().split('\n'))
    for i in temp[:-1]:
        ans.append(i.split('\t'))
    return ans

def split_video(path_video):
    name = path_video.split('/')[-1].split('.')[0]
    labels = read_from_file(f'qwe/sounds/{name}.txt')
    video = moviepy.editor.VideoFileClip(path_video)
    for i in labels:
        try:
            print(i)
            temp_video = video.subclip(float(i[0]), float(i[1]))
            temp_audio = temp_video.audio
            temp_audio.write_audiofile('temp_audio.wav')
            text = recognize_audio('temp_audio.wav')
            temp_video.write_videofile(f'qwe/short_videos/{text}.mp4')
        except:
            pass
        
for i in os.listdir('qwe/videos'):
    split_video(f'qwe/videos/{i}')

print('-------')



