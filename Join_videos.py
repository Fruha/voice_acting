import os
import moviepy.editor as mp

def read_from_file(path_txt):
    f = open(path_txt, 'r')
    ans = []
    temp = (f.read().split('\n'))
    for i in temp:
        ans.append(i.split('\t'))
    return ans

def set_duration(video, time):
	# duration = video.duration
	return video.fx(mp.vfx.speedx, video.duration/time)	

def create_end_video():
	clips = []
	labels = read_from_file('test_labels.txt')
	temp_words = os.listdir('qwe/short_videos')
	words = []
	for word in temp_words:
		words.append(word.split('.')[0])
	
	print(labels)
	
	for i in labels:
		if i[2] in words:
			clips.append(set_duration(mp.VideoFileClip(f'qwe/short_videos/{i[2]}.mp4'), float(i[1])- float(i[0])).set_start(float(i[0])))
		else:
			print("ERROR")
			print(i[2])
			exit()

	return mp.CompositeVideoClip(clips)

end_video = create_end_video()
end_video.write_videofile('end_video.mp4')



